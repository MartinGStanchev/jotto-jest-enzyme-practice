import successReducer from "./successReducer";
import { actionTypes } from "../actions";

test('returns default initial state of "false" when no action is passed', () => {
  const newState = successReducer(undefined, {});
  expect(newState).toBeFalsy();
});

test('return state of true upon receiving an action of type "CORRECT_GUESS"', () => {
  const newState = successReducer(undefined, {
    type: actionTypes.CORRECT_GUESS
  });
  expect(newState).toBeTruthy();
});
