import React, { Component } from "react";
import { connect } from "react-redux";

import Congrats from "./Components/Congrats/Congrats";
import GuessedWords from "./Components/GuessedWords/GuessedWords";
import { actionTypes } from "./actions";
import Input from "./Components/Input/Input";
class App extends Component {
  render() {
    return (
      <div className="container" data-test="component-app">
        <h1>Jotto</h1>
        <Congrats success={this.props.success}></Congrats>
        <Input />
        <GuessedWords guessedWords={this.props.guessedWords}></GuessedWords>
      </div>
    );
  }
}
const mapStateToProps = ({ guessedWords, success, secretWord }) => {
  return { guessedWords, success, secretWord };
};
const mapDispatchToProps = dispatch => {
  return {
    getSecretWord: () => dispatch({ type: actionTypes.SET_SECRET_WORD })
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
