import React from "react";
import { shallow } from "enzyme";

import { findByTestAttr, storeFactory } from "./Tests/utils";
import App from "./App";

const setup = (initialState = {}) => {
  const store = storeFactory(initialState);
  const wrapper = shallow(<App store={store} />)
    .dive()
    .dive();
  return wrapper;
};

describe("is app rendering", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup();
  });
  test("renders component without error", () => {
    const component = findByTestAttr(wrapper, "component-app");
    expect(component.length).toBe(1);
  });
});

describe("redux properties", () => {
  test("has access to secretWord", () => {
    const secretWord = "party";
    const wrapper = setup({ secretWord });
    const secretWordProp = wrapper.instance().props.secretWord;
    expect(secretWord).toBe(secretWordProp);
  });
  test("has access to success", () => {
    const success = true;
    const wrapper = setup({ success });
    const secretWordProp = wrapper.instance().props.success;
    expect(success).toBe(secretWordProp);
  });
  test("has access to guessedWords", () => {
    const guessedWords = [{ guessedWord: "train", letterMatchCount: 3 }];
    const wrapper = setup({ guessedWords });
    const guessedWordsProp = wrapper.instance().props.guessedWords;
    expect(guessedWords).toEqual(guessedWordsProp);
  });
  test("`getSecretWord` action creator in a function of props", () => {
    const wrapper = setup();
    const getSecretWord = wrapper.instance().props.getSecretWord;
    expect(getSecretWord).toBeInstanceOf(Function);
  });
});
